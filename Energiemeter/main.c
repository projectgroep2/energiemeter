/* --COPYRIGHT--,BSD_EX
 * Copyright (c) 2012, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 *******************************************************************************
 * 
 *                       MSP430 CODE EXAMPLE DISCLAIMER
 *
 * MSP430 code examples are self-contained low-level programs that typically
 * demonstrate a single peripheral function or device feature in a highly
 * concise manner. For this the code may rely on the device's power-on default
 * register values and settings such as the clock configuration and care must
 * be taken when combining code from several examples to avoid potential side
 * effects. Also see www.ti.com/grace for a GUI- and www.ti.com/msp430ware
 * for an API functional library-approach to peripheral configuration.
 *
 * --/COPYRIGHT--*/
//******************************************************************************

#include <msp430.h>
#include <stdio.h>

char ASCI[] = {' ', '!', '"', '#', '$', '%', '&', 'â‚¬', '(', ')', '*', '+', ',', '-', '.', '/', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', 'â‚¬', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', ':', ';', '<', '=', '>', '?', '@', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'â‚¬', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '[', 'â‚¬', ']', '^', '_', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'}; //Omdat het display niet de juiste ASCI tabel bezit, is hier de lijst weergegeven zodat de juiste karakters weergegeven worden.
#define WACHTTIJD 200 //Wachttijd zodat de instructies de tijd krijgen om goed uitgevoerd te worden
static int VERSTREKEN_TIJD = 0;
static int ADC_FLAG = 0;
static float SPANNING_OVER_SHUNT = 0.0;
static int VOEDINGSSPANNING = 0;
static int STROOM = 0;
static int ENERGIE = 0;
static float SHUNTWEERSTAND = 0.00625;
static int STATUS = 0b1 ;
static int RESET = 0;
static int ITTERATIES = 1;
static float voedingsspanningFloat = 0;
static float stroomFloat = 0;
static float huidigVermogenFloat = 0;
static float gemiddeldVermogenFloat = 0;
static float gemiddeldVermogenSomFloat = 0;


#pragma vector = PORT1_VECTOR
__interrupt void pin_interupt(void)
{
    if((P1IFG & BIT1) != 0)
    {
        if((P1IN & BIT1) == 0)
        {
            STATUS = 0;
        }
        else if((P1IN & BIT1) != 0)
        {
            STATUS = 1;
        }
    }
    if((P1IFG & BIT0) != 0)
    {
        RESET = 1;
    }
    P1IES ^= BIT1;
    P1IFG &= ~(BIT0 | BIT1);
}

#pragma vector = ADC10_VECTOR
__interrupt void ADCInlezen(void)
{
    if(ADC_FLAG == 0)
        {
            if(STATUS == 1)
            {
                SPANNING_OVER_SHUNT = spanningOverShunt();
            }
            ADC_FLAG = 1;
            ADC10CTL0 &= ~ENC;  // Pas nu het register aan
            ADC10CTL1 = INCH_3 | ADC10DIV_7;    // Volgende keer A3 uitlezen
            ADC10CTL0 |= ENC;
        }
    else if(ADC_FLAG == 1)
        {
            if(STATUS == 1)
            {
                VOEDINGSSPANNING = voedingsspanning();
            }
            ADC_FLAG = 0;
            ADC10CTL0 &= ~ENC;  // Pas nu het register aan
            ADC10CTL1 = INCH_2 | ADC10DIV_7;    // Volgende keer A2 uitlezen
            ADC10CTL0 |= ENC;
        }
}

int spanningOverShunt(void)
{
    static int spanningOverShunt = 0;
    static float versterkingsFactorOpAmp = 49.15;
    spanningOverShunt = ((ADC10MEM * ((2.5 * 1000) / 1023.0)) / versterkingsFactorOpAmp);
    return spanningOverShunt;
}

int voedingsspanning(void)
{
    static int voedingsspanning = 0;
    static float verzwakkingBijSpanningsdeler = 7.84;
    voedingsspanning = (ADC10MEM * ((2.5 * 1000) / 1023.0) * verzwakkingBijSpanningsdeler);
    voedingsspanningFloat = VOEDINGSSPANNING / 1000.0;
    return voedingsspanning;
}


void berekenStroom(void)
{
    if(STATUS == 1)
    {
        STROOM = SPANNING_OVER_SHUNT / SHUNTWEERSTAND;
        stroomFloat = (STROOM / 1000.0) * 0.625;
        if((stroomFloat > 0.12 && stroomFloat < 0.18) || (stroomFloat > 0.2 && stroomFloat < 3.1))
            {
                stroomFloat += 0.1;
            }
        else if(stroomFloat >= 3.1 && stroomFloat < 4.5)
            {
                stroomFloat += 0.2;
            }
        else if(stroomFloat >= 4.5)
            {
                stroomFloat += 0.5;
            }
    }
}

void berekenHuidigVermogen(void)
{
    if(STATUS == 1)
    {
        huidigVermogenFloat = voedingsspanningFloat * stroomFloat;
    }
}

#pragma vector = TIMER0_A1_VECTOR
__interrupt void verstrekenTijd(void)
{
    static int zoveelsteInterrupt = 0;
    zoveelsteInterrupt++;
    if(zoveelsteInterrupt == 2)
    {
        zoveelsteInterrupt = 0;
        if(STATUS == 1)
        {
            VERSTREKEN_TIJD++;
            berekenStroom();
            berekenHuidigVermogen();
            berekenGemiddeldVermogen();
            berekenEnergie();
        }
    }
    TA0CTL &= ~TAIFG;
}

#pragma vector = TIMER1_A1_VECTOR
__interrupt void leesADCIn(void)
{
    ADC10CTL0 |= ADC10SC;
    TA1CTL &= ~TAIFG;
}

void berekenEnergie(void)
{
    ENERGIE = gemiddeldVermogenFloat * VERSTREKEN_TIJD;
}

void berekenGemiddeldVermogen(void)
{
    gemiddeldVermogenSomFloat += huidigVermogenFloat / 1000.0;
    gemiddeldVermogenFloat = (gemiddeldVermogenSomFloat * 1000.0) / ITTERATIES;
    ITTERATIES++;
}

void zetKlokOp1MHz(void)
{
  if (CALBC1_1MHZ==0xFF)                    // If calibration constant erased
  {
      while(1);                             // do not load, trap CPU!!
  }
  DCOCTL = 0;                               // Select lowest DCOx and MODx settings
  BCSCTL1 = CALBC1_1MHZ;                    // Set range
  DCOCTL = CALDCO_1MHZ;                     // Set DCO step + modulation */
}

void stelTimer0In(void)
{
    TA0CTL = TASSEL_2 | ID_3 | MC_1 | TAIE;     // Stel Timer_0 in
    TA0CCR0 = 62500;                            // Genereert 2x p/s een interrupt voor bijhouden tijd
}

void stelTimer1In(void)
{
    TA1CTL = TASSEL_2 | ID_3 | MC_1 | TAIE;     // Stel Timer_1 in
    TA1CCR0 = 12500;                            // Genereert 10x p/s een interrupt voor het inlezen van de ADC
}

void stelADCIn(void)
{
    ADC10CTL1 = INCH_2 | ADC10DIV_7;                // ADC instellen
    ADC10CTL0 = SREF_1 | REFON | ADC10SR | REFBURST | REF2_5V | ADC10ON | ENC | ADC10IE;
}

/*** Code voor het aansturen van het display ***/
void initialiseerDisplay(void)
{
    P3DIR = 0xFF;
    P2DIR = 0x03;
    P3OUT = 0x00;
    P2OUT = 0x00;

    __delay_cycles(15000); //Wachten totdat het display is opgestart

    P2OUT &= ~BIT0;             // >
    P3OUT = 0x38;               // >>
    P2OUT |= BIT1;              // >>>
    __delay_cycles(WACHTTIJD);  // >>>>
    P2OUT &= ~BIT1;             // >>>>>
    __delay_cycles(WACHTTIJD);  // >>>>>>  Dit blok code stelt het display in als 2 lijns-display

    P2OUT &= ~BIT0;             // >
    P3OUT = 0x0C;               // >>
    P2OUT |= BIT1;              // >>>
    __delay_cycles(WACHTTIJD);  // >>>>
    P2OUT &= ~BIT1;             // >>>>>
    __delay_cycles(WACHTTIJD);  // >>>>>>  Dit blok code zet het display aan en de cursor uit

    P2OUT &= ~BIT0;             // >
    P3OUT = 0x01;               // >>
    P2OUT |= BIT1;              // >>>
    __delay_cycles(WACHTTIJD);  // >>>>
    P2OUT &= ~BIT1;             // >>>>>
    __delay_cycles(WACHTTIJD);  // >>>>>>  Dit blok code maakt het display leeg en zet de cursor linksbovenin
}

void printKarakter(int regelNummer, char karakter)
{
    P2OUT |= BIT0;              // >
    P3OUT = karakter;             // >>
    P2OUT |= BIT1;              // >>>
    __delay_cycles(WACHTTIJD);  // >>>>
    P2OUT &= ~BIT1;             // >>>>>
    __delay_cycles(WACHTTIJD);  // >>>>>> Karakter naar display schrijven

}

void printString(int regelNummer, char karakterstring[])
{
    zetCursorOpRegel(regelNummer);
    char string[20]; //Maak string aan met 20 plaatsen, voor elk digit 1 plaats
    int i;
    int element = 0;
    int nietGevonden = 999;

    for(i=0; i<20; i++)
    {
        string[i] = karakterstring[i]; //Schrijf meegegeven string in nieuwe array
    }

    for(element = 0; element<sizeof(string) && nietGevonden == 999; element++)
    {
        if(string[element] == '\0')
        {
            nietGevonden = element; //Uit de for loop stappen
        }
        else
        {
            printKarakter(regelNummer, string[element]); //Desbetreffende karakter naar display schrijven
        }
    }
}

void maakDisplayLeeg(void)
{
    P2OUT &= ~BIT0;             // >
    P3OUT = 0x01;               // >>
    P2OUT |= BIT1;              // >>>
    __delay_cycles(WACHTTIJD);  // >>>>
    P2OUT &= ~BIT1;             // >>>>>
    __delay_cycles(WACHTTIJD);  // >>>>>>  Dit blok code maakt het display leeg en zet de cursor linksbovenin
}

void zetCursorOpRegel(int regelNummer)
{
    int blijfOpRegel = regelNummer;
    if(regelNummer == 1)
    {
        regelNummer = 128;
    }
    else if(regelNummer == 2)
    {
        regelNummer = 0xC0;
    }
    else if(regelNummer == 3)
    {
        regelNummer =  0x94;
    }
    else if(regelNummer == 4)
    {
        regelNummer = 0xD4;
    }

    if(blijfOpRegel != 0) //Als regelNummer=blijfOpRegel=0, dan niet de cursor verplaatsen
    {
        P2OUT &= ~BIT0;             // >
        P3OUT = regelNummer;        // >>
        P2OUT |= BIT1;              // >>>
        __delay_cycles(WACHTTIJD);  // >>>>
        P2OUT &= ~BIT1;             // >>>>>
        __delay_cycles(WACHTTIJD);  // >>>>>>  Dit blok code zet de cursor aan het begin van de meegegeven regel
    }
}

void printInteger(int regelNummer, int integer)
{
    char integerString[6];
    int n = sprintf(integerString, "%d", integer); //Schrijf elke digit van integer als karakter in integerString
    printString(regelNummer, integerString);
}

void printFloat(int regelNummer, float Float)
{
    int tiental;
    int rest;
    int NIET_GEVONDEN = 10000;
    for(tiental=0; tiental<9999 && NIET_GEVONDEN==10000; tiental++)
    {
        if((Float/tiental)<1)
        {
            tiental -= 2;
            NIET_GEVONDEN = tiental;
        }
    }
    rest = (Float-tiental) * 1000; //Float moet een int zijn voor % operatie
    printInteger(regelNummer, tiental);
    printString(0, ".");
    printInteger(0, rest);
}
int main(void)
{
    WDTCTL = WDTPW | WDTHOLD;   // stop watchdog timer

    zetKlokOp1MHz();
    initialiseerDisplay();
    stelTimer0In();
    stelTimer1In();
    stelADCIn();

    P1DIR &= ~(BIT0 | BIT1);
    P1REN |= BIT0 | BIT1;
    P1OUT |= BIT0;
    P1OUT &= ~BIT1;

    P1IE |= BIT0 | BIT1;
    P1IES &= ~BIT1;
    P1IES |= BIT0;

    int paginaEen;
    int paginaTwee;
    int secondenPerPagina = 5;

    int j;

    __enable_interrupt();
    while(1)
    {
        if(RESET == 1)
        {
            VERSTREKEN_TIJD = 0;
            ADC_FLAG = 0;
            SPANNING_OVER_SHUNT = 0;
            VOEDINGSSPANNING = 0;
            STROOM = 0;
            ENERGIE = 0;
            ITTERATIES = 1;
            voedingsspanningFloat = 0;
            stroomFloat = 0;
            huidigVermogenFloat = 0;
            gemiddeldVermogenFloat = 0;
            gemiddeldVermogenSomFloat = 0;
            RESET = 0;
        }

        ///////////////////////////////////////// Pagina 1
        for(paginaEen=0; paginaEen<secondenPerPagina; paginaEen++)
        {
            maakDisplayLeeg();

            printString(1, "VVin: ");
            printFloat(0, voedingsspanningFloat);
            printString(0, " V");

            printString(2, "Stroom: ");
            printFloat(0, stroomFloat);
            printString(0, " A");

            printString(3, "Huid. P: ");
            printFloat(0, huidigVermogenFloat);
            printString(0, " W");

            printString(4, "Status: ");
            if(STATUS==0) printString(0, "inactief");
            else if(STATUS==1) printString(0, "actief");

            for(j=0;j<10&&RESET==0;j++)
                {
                    __delay_cycles(100000);
                }
        }

        ///////////////////////////////////////// Pagina 2
        for(paginaTwee=0; paginaTwee<secondenPerPagina; paginaTwee++)
        {
            maakDisplayLeeg();

            printString(1, "TTijd: ");
            printInteger(0, VERSTREKEN_TIJD);
            printString(0, " s");

            printString(2, "Energie: ");
            printInteger(0, ENERGIE);
            printString(0, " J");

            printString(3, "Gem. P: ");
            printFloat(0, gemiddeldVermogenFloat);
            printString(0, " W");

            printString(4, "Status: ");
            if(STATUS==0) printString(0, "inactief");
            else if(STATUS==1) printString(0, "actief");

            for(j=0;j<10&&RESET==0;j++)
                {
                    __delay_cycles(100000);
                }
        }

    }
}

